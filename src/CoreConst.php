<?php

declare(strict_types=1);

namespace GSC;


class CoreConst
{
    public const  CORE_VERSION = "1.0.0";

    public const DEFAULT_CONTROLLER = 'IndexController';

    public const DEFAULT_METHOD = 'index';

    public const SW_REQUEST = "SwRequest";

    public const SW_RESPONSE = "SwResponse";

}