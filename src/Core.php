<?php

declare(strict_types=1);

namespace GSC;

use GSC\Server\Http;
use GSC\Server\WebSocket;
use GSC\Server\MainServer;

class Core
{
    public static function logo()
    {
        $appVersion = CoreConst::CORE_VERSION;
        $swVersion  = SWOOLE_VERSION;
        $phpVersion = PHP_VERSION;

        echo <<<EOL
\e[32m
   _____  _____  _____                    _      
  / ____|/ ____|/ ____|                  | |     
 | |  __| |  __| (_____      _____   ___ | | ___ 
 | | |_ | | |_ |\___ \ \ /\ / / _ \ / _ \| |/ _ \
 | |__| | |__| |____) \ V  V / (_) | (_) | |  __/
  \_____|\_____|_____/ \_/\_/ \___/ \___/|_|\___|\e[0m
  
  Version: {$appVersion}
  PHP: {$phpVersion}
  Swoole: {$swVersion}


EOL;
    }

    public static function println($strings)
    {
        echo $strings . PHP_EOL;
    }

    public static function echoSuccess($msg)
    {
        self::println('[' . date('Y-m-d H:i:s') . '] [INFO] ' . "\033[32m{$msg}\033[0m");
    }

    public static function echoError($msg)
    {
        self::println('[' . date('Y-m-d H:i:s') . '] [ERROR] ' . "\033[31m{$msg}\033[0m");
    }

    public static function run()
    {
        self::logo();
        global $argv;
        $count    = count($argv);
        $funcName = $argv[$count - 1];
        self::parseCommand($funcName);
    }

    public static function parseCommand($command)
    {
        switch ($command) {
            case 'http:start':
                (new Http())->start();
                break;
            case 'http:stop':
                (new Http())->stop();
                break;
            case 'ws:start':
                (new WebSocket)->start();
                break;
            case 'ws:stop':
                (new WebSocket)->stop();
                break;
            case 'main:start':
                (new MainServer)->start();
                break;
            case 'main:stop':
                (new MainServer)->stop();
                break;
            default:
                self::echoError("command use [http|ws|main] [start|stop]");
                exit();
        }
    }
}
